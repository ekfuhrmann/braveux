'use strict';
var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');

gulp.task('sass', function () {
    gulp.src('./styles/dev/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(autoprefixer({remove: false}))
        .pipe(gulp.dest('./styles/dist/'));
});

gulp.task('sass-w', function() {
        gulp.watch('./styles/dev/*.scss', ['sass']);
})
