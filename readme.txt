Thanks for the assessment -- It was an enjoyable challenge to work through!

IMPORTANT: run 'npm install' within the directory to ensure that all node modules related to the project are installed .
    For working in the code, I am using a simple Gulp command to compile my SCSS into CSS which includes an auto-prefixer for multi-browser support.
    To compile the SCSS, simply type 'gulp sass', or 'gulp sass-w' if you want to have it auto-run.

I used the BEM naming convention for my CSS.  I'm still relatively new to this naming convention so it's not perfect.

My biggest weakness with front-end development is JS which I am actively working on improving.  As such I approached many elements of this project
using Pure CSS methods as opposed to JS (see menu slider).  I really only wrote JS for displaying the form and validations.

